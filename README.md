This project shows a relationship between two entities: Buildings and the rooms that they contain.

Infinite recursion in the generated JSON is prevented by defining @JsonIdentityInfo in the "building" class.

You could remove this and instead add @JsonManagedReference and @JsonBackReference to the classes. What are the differences in the generated JSON?
