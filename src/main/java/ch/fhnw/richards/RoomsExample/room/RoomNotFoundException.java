package ch.fhnw.richards.RoomsExample.room;

public class RoomNotFoundException extends RuntimeException {
    RoomNotFoundException(String roomName) {
        super("Room " + roomName + " not found");
    }
}
