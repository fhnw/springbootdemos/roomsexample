package ch.fhnw.richards.RoomsExample.room;

import java.util.Objects;

import ch.fhnw.richards.RoomsExample.building.Building;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "room")
public class Room {
    @Id @Column(name = "roomName") private String roomName;
    @Column(name = "size") private Long size;
    @ManyToOne
    @JoinColumn(name = "buildingID")
    private Building building;

    public Room() {}

    public Room(String roomName, long size) {
        this.roomName = roomName;
        this.size = size;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building building) {
        this.building = building;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Room)) return false;
        Room r = (Room) o;
        return (this.roomName.equals(r.roomName));
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.roomName);
    }

    @Override
    public String toString() {
        return "Room{" + "roomName=" + this.roomName + ", size=" + this.size + ", in building " + building.getName() + '}';
    }
}

