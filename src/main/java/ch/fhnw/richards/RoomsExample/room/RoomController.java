package ch.fhnw.richards.RoomsExample.room;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RoomController {

    private final RoomRepository repository;

    RoomController(RoomRepository repository) {
        this.repository = repository;
    }

    // List all Rooms
    @GetMapping("/rooms")
    List<Room> all() {
        return repository.findAll();
    }

    // Create a Room
    @PostMapping("/rooms")
    Room createRoom(@RequestBody Room Room) {
        return repository.save(Room);
    }

    // Read a Room
    @GetMapping("rooms/{id}")
    Room one(@PathVariable String roomName) {
        return repository.findById(roomName).orElseThrow(() -> new RoomNotFoundException(roomName));
    }

    // Update a Room (not sure if this is a sensible operation)
    // If no such Room found, then creates a new one
    @PutMapping("/rooms/{id}")
    Room updateRoom(@RequestBody Room updatedRoom, @PathVariable String roomName) {
        return repository.findById(roomName).map(Room -> {
            Room.setSize(updatedRoom.getSize());
            Room.setBuilding(updatedRoom.getBuilding());
            return repository.save(Room);
        }).orElseGet(() -> {
            updatedRoom.setRoomName(roomName);
            return repository.save(updatedRoom);
        });
    }

    // Delete a Room
    @DeleteMapping("/rooms/{id}")
    void deleteEmployee(@PathVariable String roomName) {
        repository.deleteById(roomName);
    }
}

