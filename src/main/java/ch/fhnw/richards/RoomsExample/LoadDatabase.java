package ch.fhnw.richards.RoomsExample;

import ch.fhnw.richards.RoomsExample.building.Building;
import ch.fhnw.richards.RoomsExample.building.BuildingRepository;
import ch.fhnw.richards.RoomsExample.room.Room;
import ch.fhnw.richards.RoomsExample.room.RoomRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class LoadDatabase {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(BuildingRepository br, RoomRepository rr) {

        return args -> {
            Building ori = new Building("ORI");
            log.info("Preloading " + br.save(ori));
            Building ovr = new Building("OVR");
            log.info("Preloading " + br.save(ovr));

            Room r = new Room("ORI-107", 20);
            r.setBuilding(ori);
            log.info("Preloading " + rr.save(r));
        };

    }
}

