package ch.fhnw.richards.RoomsExample.building;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BuildingController {

    private final BuildingRepository repository;

    BuildingController(BuildingRepository repository) {
        this.repository = repository;
    }

    // List all buildings
    @GetMapping("/buildings")
    List<Building> all() {
        return repository.findAll();
    }

    // Create a building
    @PostMapping("/buildings")
    Building createBuilding(@RequestBody Building building) {
        return repository.save(building);
    }

    // Read a building
    @GetMapping("buildings/{id}")
    Building one(@PathVariable Long id) {
        return repository.findById(id).orElseThrow(() -> new BuildingNotFoundException(id));
    }

    // Update a building (not sure if this is a sensible operation)
    // If no such building found, then creates a new one
    @PutMapping("/buildings/{id}")
    Building updateBuilding(@RequestBody Building updatedBuilding, @PathVariable Long id) {
        return repository.findById(id).map(building -> {
            building.setName(updatedBuilding.getName());
            building.setRooms(updatedBuilding.getRooms());
            return repository.save(building);
        }).orElseGet(() -> {
            updatedBuilding.setId(id);
            return repository.save(updatedBuilding);
        });
    }

    // Delete a building
    @DeleteMapping("/buildings/{id}")
    void deleteEmployee(@PathVariable Long id) {
        repository.deleteById(id);
    }
}

