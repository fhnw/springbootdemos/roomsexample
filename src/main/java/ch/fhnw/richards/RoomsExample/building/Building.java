package ch.fhnw.richards.RoomsExample.building;

import java.util.*;

import ch.fhnw.richards.RoomsExample.room.Room;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
@Entity
@Table(name = "building")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id"
)
public class Building {
    @Id	@Column(name = "id") @GeneratedValue private Long id;
    @Column(name = "name") private String name;
    @OneToMany(mappedBy = "building")
    private List<Room> rooms = new ArrayList<>();

    public Building() {
    }

    public Building(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Building))
            return false;
        Building b = (Building) o;
        return (this.id.equals(b.id));
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public String toString() {
        return "Building{" + "id=" + this.id + ", name=" + this.name + '}';
    }
}

