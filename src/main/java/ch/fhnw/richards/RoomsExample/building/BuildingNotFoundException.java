package ch.fhnw.richards.RoomsExample.building;

public class BuildingNotFoundException extends RuntimeException {
    BuildingNotFoundException(Long id) {
        super("Building " + id + " not found");
    }
}
