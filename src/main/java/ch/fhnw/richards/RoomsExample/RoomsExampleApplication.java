package ch.fhnw.richards.RoomsExample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoomsExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(RoomsExampleApplication.class, args);
	}

}
